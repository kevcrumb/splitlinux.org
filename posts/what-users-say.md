---
title: What Users Say
layout: default.liquid
data: 

#  2020-05-31:
#    u: ksynwa
#    q: this looks really cool. bookmarked.
#    s: #chat

  2020-06-24:
    u: the-new-guy176
    q: Awesome! I’m honestly really excited about this project!
    s: #chat

  2020-06-28:
    u: fnintr
    q: 1st time install of Split Linux. It was fun, and a pleasure to install and configure.
    s: #chat

  2020-07-07:
    u: iwahkas
    q: Very impressed. Void was a stellar choice
    long: Yeah man, I’ve been very happy and impressed with it [...] You surprised me man. [...] Void [...] was a stellar choice
    s: #chat

  2020-07-07_:
    u: iwahkas
    q: Exposed container and an isolated container as my daily driver. Love it
    long: The privacy is very important to me, I have a exposed container and an isolated container as my daily driver. Love it
    s: #chat

  2020-07-08:
    u: sysdfree Blog
    q: musl, privacy, security, encryption, and unbeatable network setup. A++
    long: |
      Split Linux (****) (all void architectures – runit) images built with musl, privacy, security, encryption, and unbeatable network setup. Its own installer. (this you have to try) A++
    s: https://sysdfree.wordpress.com/2020/07/10/321/

  2021-02-08:
    u: Jesse Smith, Distrowatch
    q: |
      The concept of Split seems to be solid and the installation went just as the documentation said it would.
      Split has a lot of potential.
    s: https://distrowatch.com/weekly.php?issue=20210208#split

  2021-08-09:
    u: Barp_the_Wire
    q: Today I discovered Split and I must say I am very impressed by all the work you have put into it.
    s: https://www.reddit.com/r/splitlinux/comments/p0yrqz/how_does_this_login_thing_work/

  2022-12-07:
    u: Cautious-Ad-1464
    q: I have never heard of split linux before but this sounds very cool! I will have to check it out
    s: https://www.reddit.com/r/splitlinux/comments/ze4euh/comment/iz83seq/

  2023-04-16:
    u: Altruistic_Comb662
    q: This Distro really deserves more attention! [it is pretty awesome.]
    s: https://www.reddit.com/r/splitlinux/comments/10c3l5v/comment/jgj43iv/
---


{% for testimonial in page.data %}
<blockquote cite="{{ testimonial[1].s }}">
<em>
  "{{ testimonial[1].q }}"<br/>
  - <a href="{{ testimonial[1].s }}">{{ testimonial[1].u }}</a>
</em>
</blockquote>
{% endfor %}
