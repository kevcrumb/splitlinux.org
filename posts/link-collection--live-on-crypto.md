---
title: 'Link collection: Live on Crypto'
layout: default.liquid
---

A link collection to help you into a free economy

---

## Stay under the radar


### Safe computer use

Split Linux :), [Split Linux](https://splitlinux.org) 

Encryption against surveillance, [PrivacyTools](https://www.privacytools.io)

Software against surveillance, [PRISM Break](https://prism-break.org)

Torified web search, [DuckDuckGo](http://www.3g2upl4pq6kufc4m.onion)

Notarize web pages, [PageSigner](https://tlsnotary.org/pagesigner.html)


### Identity protection

Create An Untraceable New Identity, [The Security Blogger article](http://www.thesecurityblogger.com/how-to-create-an-untraceable-new-identity)

Generate fake identities, [Fakename Generator](https://fakenamegenerator.com)

Generate fake names, [MuffinLabs Namey!](https://namey.muffinlabs.com)

Virtual phone numbers, [DTMF.io](https://dtmf.io)

Anonymous SMS text massaging, [VirtualSIM](https://virtualsim.net)

One-time email addresses that work for Protonmail account creation, [generator.email](https://generator.email)

Throwaway email addresses [Maildim.com](https://maildim.com)


### Communication

Torifiable VPN, [AirVPN](https://airvpn.org/?referred_by=424127) ([onion](http://airvpn3epnw2fnsbx5x2ppzjs6vxtdarldas7wjyqvhscj7x43fxylqd.onion/?referred_by=424127))

Anonymous domains, [Njal.la](https://njal.la) ([onion](http://www.njalladnspotetti.onion))

A more secure email, [Tutanota](https://tutanota.com)

Dark web site risk detection, [OnionScan](onionscan.org)

Anonymously share files of any size, [OnionShare](https://onionshare.org)

Share big files via API, [Pixeldrain](https://pixeldrain.com/api)

Immediate temporary hosting for your website, [Klawdi.com](https://klawdi.com/console)


### Darknet

Torrent tracker, [RARBG](https://rarbg.to) / [1337x](https://1337x.to) / [YTS](https://yts.lt) / [RuTracker](https://rutracker.org)

Tor site index, [dark.fail](https://dark.fail) ([onion](http://darkfailllnkf4vf.onion))

Less up-to-date Tor link list, OnionDir ([onion](http://www.dirnxxdraygbifgc.onion))


### Physical world

Secure destribution of physical goods, ["Dropgangs" article on Opaque.link](http://opaque.link/post/dropgang)


### Understand fingerprinting

Extensive fingerprinting check "Am I Unique?", [AmIUnique.org](https://www.amiunique.org/fp)

Extensive fingerprinting check "Panopticlick", [Panopticlick.EFF.org](https://panopticlick.eff.org)

Fingerprinting without JavaScript, [NoScriptFingerprint.com](https://noscriptfingerprint.com)

Explanation of fingerprinting without JavaScript, [Fingerprint.com](https://fingerprint.com/blog/disabling-javascript-wont-stop-fingerprinting)

---


## Earn crypto


### Work

Sell files for Monero, [XMRFile.com](https://xmrfile.com)

Reddit job board for tasks paid in Bitcoin, [r/Jobs4Bitcoin](https://www.reddit.com/r/Jobs4Bitcoins/?utm_source=BlockFi&utm_medium=referral&utm_campaign=resourcecenter)

Buy and sell freelance services using Bitcoin, [CryptoGrind](http://www.cryptogrind.com/?utm_source=BlockFi&utm_medium=referral&utm_campaign=resourcecenter)

Job board for projects looking to pay in crypto, [Coinality](https://coinality.com/?utm_source=BlockFi&utm_medium=referral&utm_campaign=resourcecenter)

Job board where many crypto projects are hiring, [Angel's List](https://angel.co/bitcoin/jobs?utm_source=BlockFi&utm_medium=referral&utm_campaign=resourcecenter)

Job board for freelancers that want to earn crypto for their work, [CryptoJobs](https://crypto.jobs/?utm_source=BlockFi&utm_medium=referral&utm_campaign=resourcecenter)


### Lend

Article ["Blockchain Peer to Peer (P2P) Lending"](http://moonwhale.io/crypto-collateral-p2p-lending)

Interest Account for lending BTC, [BlockFi](https://blockfi.com/crypto-interest-account)

P2P lending platform that supports XMR and EUR, [Coinloan](https://coinloan.io)

Bitcoin lending [HodlHodl](https://lend.hodlhodl.com)

---


## Spend crypto


### Travel

Around the World Tickets and Multi-Stop Flights, [AirTreks](https://www.airtreks.com)

Flights, [TravelbyBit](https://travelbybit.com)

Flights, [Greitai.lt](https://www.greitai.lt)

Flights, ~4.5% hidden fee, [Destinia](https://destinia.it)

Stays, [CryptoCribs](https://www.cryptocribs.com)

Hotels, [Travala](https://travala.com)

Hotels, [Trippki](https://trippki.com/search-trippki/)

Luxury car rental, [Lurento.com](https://lurento.com/offers/)


### Shopping services

Shop in the USA and have packages forwarded to you, [Shipito](https://www.shipito.com)

Shop in the EU and have packages forwarded to you, [ShopInBit](https://shopinbit.de/bestellservice-order-service/)


### Payment paths

Buy physical prescious metals USA, [JM Bullion](https://www.jmbullion.com)

Buy or sell physical prescious metals EU, [Bitgild](https://www.bitgild.com)

Coupons/gift cards Canada and the USA, [Coincards](https://coincards.com)

Buy virtual VISA cards that work internationally, [TheBitcoinCompany](https://thebitcoincompany.com)

Buy gift cards, [Gyft](https://gyft.com)

Gift cards and cell phone refills, [BitRefill](http://www.bitrefill.com)

Send to European Bank accounts, [CryptoTransfer.eu](https://cryptotransfer.eu)

15%+ Discount shopping Amazon with Bitcoin or Bitcoin Cash, [Purse.io](https://purse.io)

Order at HomeDepot.com or Walmart.com with 10% discount, [Alagoria](https://alagoria.com)

Business financing in 30 minutes, [Bitbond](https://bitbond.com)


### Markets

Buy and sell things online with Monero, [We Shop with Crypto](https://weshopwithcrypto.com)

Buy and sell things online with Bitcoin, [OpenBazaar](https://openbazaar.org)

Pay Bitcoin invoices with Monero, 0.3% - ~1% hidden fee, [xmr.to](https://xmr.to)

Convert between crypto currencies, ~12% down to ~0.03% hidden fees for XMR->BTC, [morphtoken.com](https://morphtoken.com)


### Hosting

VPS at rates of a shared hosting, Debian/Ubuntu, [GoVPS](https://govps.com)

VPS in Romania, Debian/Ubuntu and CentOS, [CryptoHo.st](https://cryptoho.st)

Roughly double the price of GoVPS but more operating systems to choose from, Debian/Ubuntu/Fedora/CentOS/RancherOS/FreeBSD [BitVPS](https://bitvps.com)

API-driven VPS without accounts or emails, Debian/Ubuntu/Fedora/CentOS/RancherOS/FreeBSD  [SporeStack](https://sporestack.com)


### Merchant lists

Quickly filter for services accepting Monero, [Monero Directory](https://monerodirectory.com)

Map of places accepting Monero, [XMR Coinmap at Cryptwerk](https://cryptwerk.com/coinmap/7/-34.69000526/-58.00348171/?coins=13)

Map of crypto ATM's and merchants, [Coinmap World Map](https://coinmap.org/view/#/world/)

List of companies accepting Monero, [Cryptwerk](https://cryptwerk.com/pay-with/xmr/)

Places all over the net accepting Bitcoin, [UseBitcoins.info](https://usebitcoins.info/index.php)





{% comment %}
[suspended as of june 2020] Pay any bill with Bitcoin, [Lamium](https://lamium.io/?ref=ybQK1pqH)
[unavailable as of june 2020] Buy from any online retailer, 2% service fee, [bitcoinsuperstore.us](https://bitcoinsuperstore.us)
[offline as of 2019-10] https://thebigcoin.io
[offline as of 2020-06 - same error page as thebigcoin - probably related] https://anchor-x.io Flights (Expedia & priceline.com), hotels & airbnb, groceries - without them knowing you used crypto -- careful! you pay roughy 3 - 5% extra when paying with Monero (via Coinpayments)
{% endcomment %}

