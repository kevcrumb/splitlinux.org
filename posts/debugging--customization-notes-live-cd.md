---
title: 'Debugging: Customization Notes - Live CD'
layout: default.liquid
---

This article explains where and how Split Linux deviates from a vanilla Void Live CD.


## Only 1000 LOC away

As of release 20210112 Split is *less than 1000 lines of code* away from Void. Since this number includes textual explanations like the README files verifying the code is simple.

The relevant **packages** are splitlinux-lxc-conf and splitlinux-tor-router with its Docker image of a tor-router:

    # 400 lines for packages
    cat splitlinux-*/template splitlinux-lxc-conf/files/* splitlinux-tor-router/files/splitlinux-tor-router/* | wc -l
    # 331 lines for tor-router
    cat tor-router/(README.md|build.sh) tor-router/src/*  | wc -l

The actual ISO build sources **split-mklive** make up the rest:

    # 231 lines of custumization
    git diff --stat 708e7c197f84bfdded2f75e2e12d6b5c9da88d52 7fbea40caad24486928bbc3957045950806ad07e dracut build-x86-images.sh.in keys/


## Docker 

*void-mklive/blob/master/dracut/vmklive/services.sh#L27* enables all services by default. Docker is therefore started automatically since it is installed as a dependency of splitlinux-tor-router via *build-x86-images.sh*.

The dockerized *tor-router* along with its network bridges is setup and configured by `dracut/vmklive/splitlinux.sh`.


## Log into containerized Window Manager

*split-mklive/dracut/vmklive/splitlinux.sh* (called from `module-setup.sh`) sets up an additional `lxc` group which has sudo-rights to check the status of containers (`lxc-info`), start a container (`lxc-start`) and attach to it (`lxc-attach`).

**WARNING: This allows any host-system user to attach into any other user's guest container. Until this is mitigated (maybe through using Xpra[^xpra]), Split Linux is unfit for machines used by multiple entities (as in: multiple persons using the same physical machine).**

For users belonging to the `lxc` group of the host system, `xinit` will be run upon login by the `/etc/profile.d/fork_into_container.sh` script.

{% comment %}The `anon` user is added to the `lxc` group by `adduser.sh`.{% endcomment %}

[^xpra]: https://www.xpra.org/trac/wiki/Usage


## General boot statistics

Booting from USB 3 on a low-end system takes about 65 seconds:

1. 15s - GRUB reached
2. 29s - Password prompt reached (33s if "RAM" option)
3. 16s - Boot complete		(15s if "RAM" option)
4.  5s - [Beast](/posts/the-beast-desktop-environment.html) user environment loaded
