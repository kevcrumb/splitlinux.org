---
title: 'The Beast Desktop Environment - Changelog'
layout: default.liquid
---

All Beast modules adhere to [Semantic Versioning](https://semver.org).

**beast-scripts** usually have a primary purpose with the code allowing for further specialized uses. A certain script that might be intended for hotkey use will typically offer additional modes that may be activated by passing certain parameters.

They are typically named according to a simple scheme of `<COMMAND>_<INTRUCTION>_<SUBJECT>.sh` or `<TOOL>_<DO>_<ON>.sh`. `TOOL` designates the main tool used for the operation, `DO` the intruction and `ON` whatever is affected by the operation. Here's an example:

    
     desktop/i3lock_lock_screen.sh
     │       │      │    │
     │       │      │    └─ subject ("ON")
     │       │      └─ intruction ("DO")
     │       └─ command ("TOOL")
     └─ category
    

Upon installation, the command name is converted to `B<INITIAL_OF_CATEGORY><DO>_<ON>`. Therefore `desktop/i3lock_lock_screen.sh` becomes **BDlock_screen**.

*If you like to volunteer to document all of features of a script in the form of screencasts, please get in touch for coordination. You'll benefit by obtaining in-depth understanding and maximize your efficiency with Beast.*


---

## beast-scripts 0.5.0 *(2023-05-09)*

* **csminer_mine_monero.sh:** consider BEAST_MINER_THREADS environment variable
* **maim_take_screenshot.sh:** rename from maim_screenshot.sh
* **tmux_launch_console.sh:** source XDG-directory environment variables
* new **ffmpeg_record_desktop.sh:** record videos of desktop, window or selection
* new **mv_trash_resource.sh:** move files or directories to /tmp for deletion
* new **sdiff_merge_skel.sh:** merge configuration from /etc/skel

---

## beast-scripts 0.4.0 *(2022-03-31)*

* **fzopen_update_index.sh:** make index updates more reliable
* **tmux_launch_console.sh:** correct selection of last and next month for calendar
* new **ffmpeg_extract_clip.sh:** extract clip from video or audio
* new **sxiv_browse_images.sh:** browse a directory's images as gallery
* new **sxiv_view_svg.sh:** create temporary png for fast display

---

## beast-scripts 0.3.2 *(2022-03-16)*

* **fzopen_update_index.sh:** create incrontab if it doesn't exist
* **gzy_fuzzy_find.sh:** do not cd when jump is cancelled (ctrl-c)
* **tmux_launch_console.sh:** start rover with XDG directories opened in registers 4-8
* new **torbrowser_launch_browser.sh:** launch tor-browser with --allow-remote --new-tab

---

## beast-scripts 0.3.1 *(2022-03-02)*

* **tmux_launch_console.sh:** replace "while 1" by "while true"
* **maim_screenshot.sh:** ensure valid file names (using inline-detox & cut)
* **tmux_toggle_window.sh:** launch default Beast Console if no session found (BDlaunch_console instead of "|| tmux")
* **feh_randomize_wallpaper.sh:** make wallpaper change immediate by calling BDupdate_statusbar

---

## beast-scripts 0.3.0 *(2021-06-11)*

* **dmenu_paste_snippet.sh:** select custom colors through env vars BD_DMENU_COLOR1 and BD_DMENU_COLOR2
* **pass_manage_passwords.sh:** select custom colors through env vars BD_DMENU_COLOR1 and BD_DMENU_COLOR2
* new **eyed3_show_id3.sh:** quickly access a songs ID3 information 
* new **mpc_play_track.sh:** offer quick song selection for mpd 

---

## beast-scripts 0.2.2 *(2021-05-05)*

* **pass_manage_passwords.sh:** offer dmenu filtering if auto-login match is ambiguous and add sanity check to paste_and_clear_selection (only single-line username + password shall be pasted)
* **tmux_launch_console.sh:** refactor for looping executables in a DRY manner and replace extra shell by rover window and ditch problematic tmux hook

---

## beast-scripts 0.2.1 *(2020-12-18)*

* **tmux_launch_console.sh:** improve a pane's wording
* **tmux_toggle_window.sh:** maintain VIM-window layout across resizes and don't switch tmux window unless tmux X window is active

---

## beast-scripts 0.2.0 *(2020-12-10)*

* new **fzopen_update_index.sh:** create and maintain an index of files and directories

---

## beast-scripts 0.1.1 *(2020-12-10)*

* **dmenu_paste_snippet.sh:** turn ~/.config/beast/snippets file into directory and use all files inside of it
* **feh_randomize_wallpaper.sh:** fix maxdepth/recursion
* **gzy_fuzzy_find.sh** enable smart-case filtering and ditch ag in favor of grep
* **tmux_launch_console.sh:** replace XDG_* in scripts by call to `xdg-user-dir ...` and improve finance-related panes

---

## beast-scripts 0.1.0 *(2020-11-28)*

* new **csminer_mine_monero.sh:** donate hashrate easily or mine for oneself
* new **date_log_instant.sh:** quickly log date and time (intended for hotkey use)
* new **tmux_launch_console.sh:** launch the Beast Console (Beast's main user interface)
* new **cat_get_battery_charge.sh:** evaluate current battery charge (intended for display in statusbar)
* new **maim_screenshot.sh:** take screenshots of window, fullscreen or cursor-selection (intended for hotkey use)
* new **xsetroot_update_statusbar.sh:** periodically update status bar with useful information
* new **gzy_fuzzy_find.sh:** filter for contents blazingly fast
* new **pass_manage_passwords.sh:** manage credentials and the URL's where they apply
* new **feh_randomize_wallpaper.sh:** select and set a wallpaper from a given set (see .config/beast/backgrounds/README)
* new **dmenu_paste_snippet.sh:** quickly select and paste text snippets and clipboard content
* new **tmux_toggle_window.sh:** jump to specific Beast Console windows (intended for hotkey use)
* new **i3lock_lock_screen.sh:** lock the session and show a picture (see .config/beast/lockscreens/README)

---
