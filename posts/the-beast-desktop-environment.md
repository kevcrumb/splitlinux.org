---
title: 'The Beast Desktop Environment'
layout: default.liquid
---

---

<img src="/images/beast_fullscreen1.png" width="40%"
     alt="Beast console tmux in dwm"
     title="Beast console tmux in dwm"/>
<img src="/images/beast_fullscreen2.png" width="40%"
     alt="Beast showing rover, neofetch and Monero client"
     title="Beast showing rover, neofetch and Monero client"/>

---

## What is Beast?

Throughout my lifetime I was seeking to design working with a computer as efficient as possible.

The quest took me from finding the optimal operating system to evaluating the best application for each and every task to scripting thousands of lines in order to shorten the path for any task imaginable[^scripts].

From navigating the file system to managing finances to video editing, everything had to be as fast as possible with tools based on sane concepts that don't get in the way. It was an obsession.

In order to be able to work efficiently it's crucial to understand what's really needed and what can and should be discarded from a process. I was open to internalize that knowledge, understanding that retaining that knowledge will only be possible if different tools worked in similar fashion. This is a key ingredient for making it viable for others to learn how to use such a system.

Beast is the result of destilling those insights into a complete desktop environment. It may look stripped-down at first, but if you really dedicate time to master it, you'll see that it doesn't leave anything to be desired.

Remember that Beast is built on the survivalists premise: 

 *"The more you carry in your head, the less you have to carry on your back."*

Beast heavily relies on the command line. It has a steep learning curve, but tries to make adopting the next application easier by utilizing those that maintain similar key combinations. Be it email, the music player or the console, every tool where it makes sense is **VIM-style**.

In the end Beast is what I wish they had have me learn as a teenager, teaching me the priceless value of the **UNIX philosophy** early on while sparing me the hassle of wandering an endless jungle of tools to finally find those that suck the least.

**If you're up for the challenge, maybe Beast can be this for you - the desktop environment the hacker in you always dreamt of.**

[^scripts]: A selection of those scripts is already included in Beast. Further ones will be added over time. Be sure to keep an eye on [Beast's changelog](/posts/the-beast-desktop-environment--changelog.html).


---

## Keyboard modifier levels

_(defined in [config.h](https://gitlab.com/splitlinux/split-packages/-/blob/master/srcpkgs/beast-dwm/files/config.h))_


### Alt (interface control and navigation)

![Layout for Alt-key combinations](/images/layout1_alt.png)

### Super (application launch and selection)

![Layout for Super-key combinations](/images/layout2_super.png)

### Super-Alt (audio and video control)

![Layout for SuperAlt-key combinations](/images/layout3_super_alt.png)

---
