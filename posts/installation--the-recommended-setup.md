---
title: 'Installation: The Recommended Setup'
layout: default.liquid
data:
  redirect: https://docs.splitlinux.org/split-linux/installation/the-recommended-setup.html
---

This page is now part of the Split Linux Handbook.

Visit <{{ page.data.redirect }}>.
