---
title: 'Configuration: Internet for Applications'
layout: default.liquid
data:
  redirect: https://docs.splitlinux.org/split-linux/configuration/internet-for-applications.html
---

This page is now part of the Split Linux Handbook.

Visit <{{ page.data.redirect }}>.
