---
title: 'Link collection: Enter Crypto Currencies'
layout: default.liquid
---

A link collection to help you around crypto currencies

---



## Understand and Explain

Why cryptocurrencies exist, what they do and why they matter, [WhyCryptocurrencies.com](https://whycryptocurrencies.com)

The Best Privacy Coins for Anonymous Payments, [KeySheet.io article](https://www.keysheet.io/guides/best-privacy-coin)

Table of M0 money supply by country, [Trading Economics](https://tradingeconomics.com/country-list/money-supply-m0)


### Monero

Marketing Resources, [Monero Outreach](https://www.monerooutreach.org/)

Multisignatures Explained, [Hackernoon article by Exantech](https://hackernoon.com/monero-multisignatures-explained-46b247b098a7)

Confidential Transactions, [GoodAudience article by Exantech](https://blog.goodaudience.com/monero-confidential-transactions-or-send-i-know-not-what-to-someone-i-know-not-whither-337f20f0d64e)

Knowledge Test, [Monero Certification Test](https://docs.google.com/forms/d/1xvqzXPfvKkuo0jkudw97vxOZelRD0aHaHiEaYcO6NOI/edit)

---


## Explore and Analyze


### Visualization

Visualize market dominance in blocks, [Coin360](https://coin360.com)

Multiple asset charts within one view, [Cryptowat.ch](https://cryptowat.ch/cards/assets)

Quick view on Buy/Sell pressure, [CryptoMeter.io](https://www.cryptometer.io/data/binance/xmr/btc)


### Research

Ad-free cryptocurrencies market price plots with API, [CryptoMarketPlot](https://cryptomarketplot.com) ([onion](http://4vhxreysjshbfrib.onion))

Crypto research platform with extensive filtering, [Messari Screener](https://messari.io/screener)

Asset rating based on buy support, [CoinMarketBook](https://coinmarketbook.cc)

Crypto research platform, [CoinCheckup](https://coincheckup.com)


### Tools

Threshold E-Mail alerts, [CoinWink](https://coinwink.com)

Free API for coin metrics, [CoinMetrics Community Network Data](https://coinmetrics.io/community-network-data/)

Cryptocurrency statistics, [BitInfoCharts](https://bitinfocharts.com)

Monero Block Explorer, [XMRChain.net](https://xmrchain.net)

Bitcoin chain analysis, [Open eXploration Tool](https://oxt.me)


### Exchanges

Exchanges ranked by KYC rules, [KYCNOT.ME](https://kycnot.me)

Exchanges ranked by trading fees, [CryptoFeeSaver](https://www.cryptofeesaver.com/exchanges/fees/coins/monero)

Instant cryptocurrency exchange, [FixedFloat.io](https://fixedfloat.com/)

Instant cryptocurrency exchange, [SwapSwop.io](https://swapswop.io/)
