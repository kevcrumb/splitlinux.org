---
title: The Split Way
layout: default.liquid
---

*"One way to reduce correlations of your activity is to use privacy tools all the time, not just when you need them. This provides plausible deniability."[^hoaxer]*

Even your encrypted communication is stored in order to be deciphered once technology permits. Protect yourself by detaching your real identity from your online activities.

When combined with a set of operational principles Split Linux's technical advantages unfold their full potential. *The Split Way.*


## Split Linux Technical Benefits

These benefits are largely specific to a Split Linux setup. Out-of-the-box most other distributions will not provide them.

- Login several X users at a time.
- Avoid traffic from applications being correlated ("Stream-Isolation").
- Use multiple isolated online identities without reboots.
- Have containers utilize wireless networks.
- Use any Linux distribution for any user / identity.
- Run untrusted software in isolated environment.
- Simplify backups even for entire identities.
- Avoid people accidentally seeing content they should not.
- Browse the darknet out-of-the-box (Tor/.onion).
- Keep additional identities on encrypted external devices.
- Avoid the need of booting separate live media to fix errors.

{% comment %}
- i2p
- circumvent censorship in the clearnet
- snapshots
- LXC clones for simplified administration https://wiki.archlinux.org/index.php/Linux_Containers#LXC_clones
- Measure time spent in each identity.
{% endcomment %}


## Split Linux Operational Principles

While these concepts are easily aplicable in other distributions, the Split Way of separation helps to remember their application.

- Configure each user workspace for maximum efficiency.
- Completely separate work for different endeavors/companies.
- Work in a focussed manner within one user / identity.
- Reduce risk when allowing others to use your computer.
- Easily hand over any and all data to a successor.
- Remind loved ones to always use fake data when online.
- Present your views through pristine online identities and be judged based on your content, not your popularity.


---

[^hoaxer]: [Why Tor failed to hide the bomb hoaxer at Harvard](https://theprivacyblog.com/anonymity/why-tor-failed-to-hide-the-bomb-hoaxer-at-harvard/)|
