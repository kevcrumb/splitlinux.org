---
title: 'Installation: Creating A Custom Container (Optional)'
layout: default.liquid
data:
  redirect: https://docs.splitlinux.org/split-linux/installation/creating-a-custom-container.html
---

This page is now part of the Split Linux Handbook.

Visit <{{ page.data.redirect }}>.
