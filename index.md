---
layout: default.liquid
title: Split Linux - Just give me the light!
---

Split Linux is a general operating system optimized for safely navigating hostile environments like the Internet and physical check points.

> _"musl, privacy, security, encryption, and unbeatable network setup. A++"<br/>
> -- sysdfree Blog_

Split Linux builds on tools that follow the UNIX philosophy and is based on the fast and independent Void Linux.

{% comment %}
It is also the only Linux distribution that uses [DWM](https://dwm.suckless.org/) as default desktop for containers and ships with [Monero](https://web.getmonero.org/) to satisfy your financial needs right from the get-go.
{% endcomment %}

Read [The Split Way](/posts/the-split-way.html) to learn about its benefits and philosophy.

---


## Hard disk interaction

<img src="images/blockdevice_layout.png" alt="Blockdevice Layout" title="Blockdevice Layout" width="100%"/>

When booting the Split Linux live CD/ISO:

1. If present, the user is prompted ot unlock a `crypto_LUKS`-type partition.
2. If a volume group named **"split"** is found, it is activated.
3. If a logical volume named **"horde"** is found, it is mounted on `/var/lib/lxc`.
4. When the user logs in with a name matching one of the available containers, they directly end up in a session *within* that container.
    - This will typically be an Xorg application like a window manager or desktop environment
5. The user may launch additional containers in any other TTY and switch between them using <kbd>Ctrl-Alt-Fx</kbd>.

Containers can be any Linux distribution of your choice while VM's can be a wide range of operating systems, including Linux.

The decoy OS could be your current operating system resized to make space for the encrypted part, simply something small and innocent-looking or be left out altogether.

---


## Networking in Split Linux Containers

<img src="images/networking.graphviz.dot.png" alt="Networking in Split Linux Containers" title="Networking in Split Linux Containers" width="100%"/>

Split Linux launches a dockerized Tor router at boot. Containers connect through this router to the Internet in one of two fashions:


### Isolated

In the recommended *isolated* configuration a container does not have a default gateway configured. Each application has to be told to use the Tor router as proxy. This way, the connections of various applications cannot be related to one another. Measures are in place to make sure that each application uses a completely separate Tor circuit.

Isolated should be the default as it provides the highest level of anonymity.


### Transparent

The *transparent* setup is not recommended. The Tor router is defined as default gateway in the container and any application can access the Internet without additional configuration. The Tor circuits still differ from those of other containers, avoiding them to be related to each other, but the activity of one application may be correlated to that of another. It is still possible to configure applications to use separate circuits though.

Transparent might be used in the beginning until you know how to connect your applications.


### Other modes

The user may opt to circumvent the use of Tor altogether, ditching anonymity for containers where Internet connection speed is more important than stealth. As a middle ground, traffic may still be routed through a VPN.

---

<a name="download"></a> 
## Download and Install

Download [split-live-x86_64-musl-current.iso](https://kevcrumb.gitlab.io/split-mklive/split-live-x86_64-musl-current.iso).

Then verify checksum and flash:

    sha256sum split-live-x86_64-musl-current.iso |
      grep 1e4be74189b98570f471de21fc9c46bece6b3ca9dc298fdf502acbde95dcf624 &&
        dd status=progress oflag=direct bs=2M \
	       if=split-live-x86_64-musl-current.iso \
	       of=/dev/disk/by-id/<DEVICE>

Replace `<DEVICE>` with the path to your pendrive. For added certainty cross-check the SHA256-sum shown here with the one published on Reddit.


{% comment %}
Download signing key, signature and the actual ISO:

- [splitlinux.pub](/splitlinux.pub)
- [split-live-x86_64-musl-20200531.iso.sig](/split-live-x86_64-musl-20200531.iso.sig)
- [split-live-x86_64-musl-current.iso](https://kevcrumb.gitlab.io/split-mklive/split-live-x86_64-musl-current.iso)

With *split-live-x86_64-musl-current.iso.sig*, *split-live-x86_64-musl-current.iso* and *splitlinux.pub* in the same directory verify your download like so:

`signify -V -p splitlinux.pub -m split-live-x86_64-musl-current.iso`

The content of *splitlinux.pub* should be:

    untrusted comment: signify public key
    RWS7rD/YppEtQafeyP3cCYd+5XYkLCJi+w5noCI6KGfRClNjcpqQ6Pd+
{% endcomment %}

---

## Release history

Expect releases once to twice a year. You'll be working from containers which you can update according to your needs.

- [6.6.11](https://gitlab.com/kevcrumb/split-mklive/-/compare/v6.3.13...v6.6.11?from_project_id=19083445&straight=true) - *split-live-x86_64-musl-20240128.iso*
  - `1e4be74189b98570f471de21fc9c46bece6b3ca9dc298fdf502acbde95dcf624`
  - new command `splt` establishes an extensible interface to frequently used actions
    - The sub-commands `create`, `info`, `start`, `attach`, `route`, `vethup` and `restart` are available.
    - `splt create void <arguments>` creates a new Void Linux container.
    - `splt route` permanently switches containers between the established Split Linux networking styles "isolated", "leaky" and "exposed".
    - `splt vethup` enables the virtual networking interfaces of active containers.
  - distributions based on *BusyBox* may now be logged into directly
    - The shell configured for the container-user must be installed for GUI-launch to succeed.
  - virtual network interfaces for new containers are now easily identified by their name being `veth-<CONTAINER_NAME>`
    - Interface-names of existing containers can be set by adding a line like `lxc.net.0.veth.pair = veth-<CONTAINER_NAME>` to their **config** file.
  - new soft-link */HOST* allows quick access to contents of `/var/lib/lxc/_host/`
  - Tor router updated to version 0.4.8.10
- [6.3.13](https://gitlab.com/kevcrumb/split-mklive/-/compare/v6.1.4...v6.3.13?from_project_id=19083445&straight=true) - *split-live-x86_64-musl-20230823.iso*
  - `3152791b9f0f055390bb24ca7dced624edc31b0f8040dfb3ab10ac70ead1a257`
  - connections to Tor services may now utilize [Client Authorization](https://community.torproject.org/onion-services/advanced/client-auth/)
    - Keys formatted `<something>.auth_private` and owned by group `9001` have to be placed into `/_host/override/tor_client_auth/`.
    - The `splitlinux-tor-router` service has to be restarted for changes to take effect.
  - torified DNS queries are now possible for containers of type "exposed"
    - Requests are handled on IP `172.17.0.2` through UDP port 53.
  - files added by Split Linux itself can now be replaced through overrides
  - Tor router updated to version 0.4.7.13
- [6.1.4](https://gitlab.com/kevcrumb/split-mklive/-/compare/v5.15.30...v6.1.4?from_project_id=19083445&straight=true) - *split-live-x86_64-musl-20230114.iso*
  - `26bd661e10921fd2b9d17a7a8da1571067948424f985c08922e53559c5609566`
  - swap volumes named "`split-swap`" are now automatically used as such
  - support for 1 GB huge pages is activated
    - Configuration is done by overriding `sysctl.conf`, `rc.local` and `limits.conf` as required.
  - Tor router updated to version 0.4.7.10
- [5.15.30](https://gitlab.com/kevcrumb/split-mklive/-/compare/80f82a13da6737f16877a723e1d0dfe69b50d14e...v5.15.30?from_project_id=19083445&straight=true) - *split-live-x86_64-musl-20220326.iso*
  - `7161f6ac83e2796b79e6d4369b749ce414aec5f4a25ba09d02e645a229ec623a`
  - encrypted RAID-devices are included in the selection of split-devices
  - devices labelled "`split*`" are preferred for decryption at boot
  - command `format_for_splitlinux` labels newly created devices accordingly
    - Existing devices can be named by issuing `cryptsetup config --label split <DEVICE>`.
  - container-restart from `create_voidlinux_container` should now be faster
- *split-live-x86_64-musl-20220215.iso*
  - `a6cadc3677bcf704639a90a35aa694d5ff47b2ad4eabcd56d88aa25b3933c2f8`
  - immediate login after system boot is no longer hindered by slow Docker service startup
  - networks may now route through a total of five SOCKS ports each
    - `172.17.0.2:9050..9054` for identities in "exposed"
    - `172.18.0.2:9050..9054` for identities in "isolated"
    - `172.20.0.2:9050..9054` for identities in "leaky"
  - closing graphical session in the background no longer pulls the focus to its tty (upstream improvement)
  - Tor router updated to version 0.4.6.9
- *split-live-x86_64-musl-20220213.iso*
  - `5b8992d429d3c0d866b2b98ea2e4c5d565294f0a4a9fc6b6d1da727342ca2d86`
  - *retracted - see next release instead*
- *split-live-x86_64-musl-20210506.iso*
  - `7088fd01b00eaf962c85ba11ed4f1fc934566049461d89144d3d492adf8255d6`
  - output of splitlinux-tor-router service no longer spams the console
    - The section ["Logging"](https://docs.voidlinux.org/config/services/logging.html) in the Void Linux Handbook explains how to access the information.
  - new command `format_for_splitlinux` automates [hard disk preparation](/posts/installation--the-recommended-setup.html)
  - root now uses bash as shell; if no container partition is mounted, format_for_splitlinux is mentioned upon login
  - Tor router updated to version 0.4.4.8
- *split-live-x86_64-musl-20210112.iso*
  - `1ee709b2ab9c547105f73d3c47d417b9e2c2b6328717f598cdfc3539c762b231`
  - network link is explicitly activated on container start
  - crypt device selection now handles non-numeric input graciously
- *split-live-x86_64-musl-20201128.iso*
  - `0681621d58090567555fe7a73f791fbba769b435cd1c1793d7a106970c770d1a`
  - new command `create_voidlinux_container` simplifies [creation of new identities](/posts/installation--the-recommended-setup.html)
  - feral surprise added to repos (subsequently announced as the [Beast Desktop Environment](/posts/the-beast-desktop-environment.html))
- *split-live-x86_64-musl-20201108.iso*
  - `aafc067e3e3f3b2e2567a1b2a1f52b9c269b9dc4959ed2b22549331cf9e73fcf`
  - user is presented with Split Linux specifc hints at login
  - passwords for **root** and **anon** are overridden if a "shadow-formatted" password is present in `/_host/password` on the *horde* volume
  - host system's `/etc` files will be overridden by files from `/_host/override/etc/` of the *horde* volume; ideas:
    - Configure a `wpa_supplicant/wpa_supplicant-<interface>.conf` to automatically connect wireless on boot.
    - Configure a `udev/rules.d/99-sound-for-containers.rules` to allow sound from containers.
    - Configure a `X11/xorg.d/70-synaptics.conf` to customize touchpad behaviour.
- *split-live-x86_64-musl-20201104.iso*
  - `ca3c88467520907f05f3e571f176a71f2ead0056e2f3fb68ed4bcc43aa54d17f`
  - "leaky" network configuration now working
  - Tor router updated to version 0.4.3.5
- *split-live-x86_64-musl-20200701.iso*
  - `a6e925d3e0c5c1b91cba0a6d96924191664f8b9483b390396708f899e31a7cae`
  - clearnet routing corrected
  - package cache no longer shared with host
  - container .xinitrc no longer needs to be made executable
- *split-live-x86_64-musl-20200531.iso*
  - `d6530d730f0fd11eb0230dded60b4f874213c3f4359ff27f9a5f445c9ce207b8`
  - initial release

---


## Documentation

The Split Handbook ([HTML](https://docs.splitlinux.org), [PDF](https://docs.splitlinux.org/splitlinux-handbook.pdf))

---


## Notes

{% for post in collections.posts.pages %}
{% unless post.data.redirect %}

[{{ post.title }}]({{ post.permalink }})
{% endunless %}
{% endfor %}

---


## Join the community

- Reddit [r/splitlinux](https://www.reddit.com/r/splitlinux/)

---


![Logo][logo]


[logo]: images/logo.png
{% comment %}
[networking]: images/networking.graphviz.dot.png
[blockdevice_layout]: images/blockdevice_layout.png
{% endcomment %}
